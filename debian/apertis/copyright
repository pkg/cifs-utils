Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2008, 2012, Jeff Layton (jlayton@samba.org)
License: GPL-3+

Files: autom4te.cache/* contrib/* doc/*
Copyright: 2001, Andrew Tridgell
 2001, Andrew Bartlett
 2002, Jim McDonough
 2003, Luke Howard
 2003,2008, Steve French
 2007, Igor Mammedov
 2008, Jeremy Allison
 2008-2009, Jeff Layton
License: GPL-3+

Files: INSTALL
Copyright: 1994-1996, 1999-2002, 2004-2013, Free Software Foundation
License: FSFAP

Files: aclocal.m4
Copyright: 1996-2014, Free Software Foundation, Inc.
License: (FSFULLR and/or GPL-2+) with Autoconf-data exception

Files: asn1.c
 asn1.h
Copyright: 2001, Andrew Tridgell
License: GPL-3+

Files: checkopts
Copyright: 2018, Aurelien Aptel (aaptel@suse.com)
License: GPL-3+

Files: cifs.idmap.c
 cifsacl.h
 getcifsacl.c
 setcifsacl.c
Copyright: 2011, Shirish Pargaonkar (shirishp@us.ibm.com)
License: GPL-2+

Files: cifs.upcall.c
Copyright: 2010, Jeff Layton (jlayton@samba.org)
 2007, Igor Mammedov (niallain@gmail.com)
License: GPL-2+

Files: cifs_spnego.h
Copyright: 2007, Red Hat, Inc.
License: LGPL-2.1+

Files: cifscreds.c
 cifskey.c
 cifskey.h
 resolve_host.c
 resolve_host.h
Copyright: 2010, Jeff Layton (jlayton@samba.org)
 2010, Igor Druzhinin (jaxbrigs@gmail.com)
License: GPL-3+

Files: compile
 depcomp
 missing
Copyright: 1996-2014, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL

Files: data_blob.c
Copyright: 2001, Andrew Tridgell
 2001, Andrew Bartlett
License: GPL-3+

Files: data_blob.h
Copyright: no-info-found
License: GPL-3+

Files: debian/*
Copyright: 2002-2004, Eloy Paris <peloy@debian.org>
            2004-2010, Steve Langasek <vorlon@debian.org>
            2005, Noèl Köthe <noel@debian.org>
            2006, Peter Eisentraut <petere@debian.org>
            2008-2010, Christian Perrier <bubulle@debian.org>
License: GPL-3+

Files: install-sh
Copyright: 1994, X Consortium
License: X11

Files: mount.cifs.c
Copyright: 2010, Jeff Layton (jlayton@samba.org)
 2008, Jeremy Allison (jra@samba.org)
 2003, 2010, Steve French (sfrench@us.ibm.com)
License: GPL-3+

Files: pam_cifscreds.c
Copyright: 2013, Orion Poplawski <orion@cora.nwra.com>
 2007, Stef Walter
License: GPL-3+

Files: smb2-quota
Copyright: 2019, Ronnie Sahlberg (lsahlberg@redhat.com)
 2019, Kenneth Dsouza (kdsouza@redhat.com)
License: GPL-2+

Files: smbinfo
Copyright: 2019, Ronnie Sahlberg <lsahlberg@redhat.com>
 2019, Aurelien Aptel <aaptel@suse.com>
License: GPL-2+

Files: spnego.c
 spnego.h
Copyright: 2003, Luke Howard
 2002, Jim McDonough <jmcd@us.ibm.com>
 2001, Andrew Tridgell
License: GPL-3+

Files: util.c
 util.h
Copyright: 2005-2008, Jelmer Vernooij
 1992-1998, Andrew Tridgell
License: LGPL-3+

Files: Makefile.in
Copyright: 2001, Andrew Tridgell
 2001, Andrew Bartlett
 2002, Jim McDonough
 2003, Luke Howard
 2003,2008, Steve French
 2007, Igor Mammedov
 2008, Jeremy Allison
 2008-2009, Jeff Layton
License: GPL-3+
